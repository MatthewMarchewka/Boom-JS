function _Square(pozX,pozY, type) {
    this.square = null
    this.image = null
    this.pozX = pozX
    this.pozY = pozY
    this.type = type
    this.additional = null
    this.index

    this.create = function () {
        this.square = document.createElement("div")
        this.square.classList.add("square")
        this.square.style.left = this.pozX * 33 + "px"
        this.square.style.top = this.pozY * 33 + "px"
        if(this.type != null){
            this.img= document.createElement("img");
            this.img.src= "gfx/"+this.type+".png"
            this.square.append(this.img)
            mapJson.level.push({x: this.pozX, z: this.pozY , type: this.type})
            document.getElementById("generatedJson").value = JSON.stringify(mapJson).replace(/,/g,",\n")
            this.index = mapJson.level.length - 1
        }
        function setType(object,click){
            if(object.type)
                mapJson.level[object.index].type = mode
            else{
                mapJson.level.push({x: object.pozX, z: object.pozY , type: mode})
                object.index = mapJson.level.length - 1
            }
            object.type = mode
            if(object.img)
                object.square.removeChild(object.img)
            object.img= document.createElement("img");
            object.img.src= "gfx/"+object.type+".png"
            object.square.append(object.img)
            document.getElementById("generatedJson").value = JSON.stringify(mapJson).replace(/,/g,",\n")
        }
        this.square.addEventListener("click", setType.bind(null,this) );
        document.getElementById("previewDiv").append(this.square)
    }




    this.create();
}