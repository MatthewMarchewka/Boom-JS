mapJson = {
    room: 0,
    width: 0,
    height: 0,
    level: []
}
var buttons = ["Wall","Path", "Enemy", "Doors", "Light", "Ornament"]
mode = "Wall"

var gridGenerate = function(width,height){
    document.getElementById("previewDiv").innerHTML = ""
    mapJson.width = width
    mapJson.height = height
    mapJson.level = []
    for(var y=0; y<height; y++){
        for(var x=0; x<width; x++){
            var square = new _Square(x,y, null)
        }
    }
}

function configFill(){
    var buttonDiv = document.createElement("div")
    buttonDiv.id = "buttonDiv"
    document.getElementById("config").append(buttonDiv)

    for(var i=0; i< buttons.length; i++){
        var button =  document.createElement("button")
        button.id = buttons[i]
        button.innerHTML = buttons[i]
        button.addEventListener("click", function(){
            mode = this.innerHTML
        })
        //dodać listener do buttona
        buttonDiv.append(button)
    }
}

function previewFill(){
    var previewDiv = document.createElement("div")
    previewDiv.id = "previewDiv"
    document.getElementById("preview").append(previewDiv)
}

function jsonPreviewFill(){
    //textarea for generated json
    var generatedJson = document.createElement("textarea")
    generatedJson.id = "generatedJson"
    document.getElementById("jsonPreview").appendChild(generatedJson)
    //button for copying to clipboard
    var clipboardButton = document.createElement("button")
    clipboardButton.id = "clipboard"
    clipboardButton.innerHTML = "Select text"
    clipboardButton.addEventListener("click", function(){
        generatedJson.select()
    })
    document.getElementById("jsonPreview").append(clipboardButton)

    var generateButton = document.createElement("button")
    generateButton.id = "generate"
    generateButton.innerHTML = "Generate Grid"
    generateButton.addEventListener("click", function(){
        var generateField = JSON.parse(document.getElementById("generatedJson").value)
        gridGenerate(generateField.width, generateField.height)
        document.getElementById("generatedJson").value = ""
        for(var i=0; i<generateField.level.length; i++)
            var square = new _Square( generateField.level[i].x, generateField.level[i].z , generateField.level[i].type)

    })
    document.getElementById("jsonPreview").append(generateButton)

}


document.addEventListener("DOMContentLoaded", function(event){
    configFill()
    previewFill()
    jsonPreviewFill()
    gridGenerate(document.getElementById("width").value, document.getElementById("height").value)
    document.getElementById("submit").addEventListener("click", function(){
        document.getElementById("generatedJson").value = ""
        gridGenerate(document.getElementById("width").value, document.getElementById("height").value)
    })
})