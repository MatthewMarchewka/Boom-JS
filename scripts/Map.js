function _Map() {

    var canvasMinimap
    var canvasPlayerOnMinimap
    var contentMapDiv
    var minimapScale
    var playerInfo

    this.update = function(){
        this.fillCanvasMapDiv()
        this.fillCanvasPlayerOnMinimap()
    }

    this.fillCanvasMapDiv = function () {
        var ctx = canvasMinimap.getContext('2d');
        ctx.clearRect(0, 0, canvasMinimap.width, canvasMinimap.height);
        ctx.fillStyle = 'rgb(0,0,0)';
        ctx.fillRect(0, 0, canvasMinimap.width, canvasMinimap.height);
        var i,j = 0
        for (var y = Math.floor(player.y)-3; y < Math.floor(player.y)+4; y++) {
            i = 0
            for (var x = Math.floor(player.x)-3; x < Math.floor(player.x)+4; x++) {
                if( (0 > y || 0 > x) || (y > level.mapHeight-1 || x > level.mapWidth-1) )
                    var wall = 1
                else
                    var wall = level.getLevel()[y][x]; 
                // If there is a wall block at this (x,y)…
                if (wall > 0) {
                    var patternMiddle = ctx.createPattern(Variables.mapWallTexture, "repeat")
                    ctx.fillStyle = patternMiddle;
                    // …Then draw a block on the minimap
                    ctx.fillRect(i * minimapScale, j * minimapScale, minimapScale, minimapScale);
                }
                i++
            }
            j++
        }
    }

    this.fillCanvasPlayerOnMinimap = function(){
        var ctx = canvasPlayerOnMinimap.getContext('2d');
        ctx.clearRect(0, 0, canvasPlayerOnMinimap.width, canvasPlayerOnMinimap.height);
        ctx.save();
        ctx.translate((canvasPlayerOnMinimap.width/2), (canvasPlayerOnMinimap.height/2))
        ctx.rotate(player.rotation)
        ctx.drawImage(Variables.mapPlayerTexture, -1*(minimapScale/2) , -1*(minimapScale/2), minimapScale*1, minimapScale*1)
        ctx.restore()
    }

    this.drawMinimap = function(){
        minimapScale = canvasPlayerOnMinimap.width / 7
        this.fillCanvasMapDiv()
        this.fillCanvasPlayerOnMinimap()
    }

    this.setCanvasSize = function(){
        canvasMinimap.width = contentMapDiv.offsetWidth - 60
        canvasMinimap.height = canvasMinimap.width
        canvasPlayerOnMinimap.width = contentMapDiv.offsetWidth - 60
        canvasPlayerOnMinimap.height = canvasPlayerOnMinimap.width
        this.drawMinimap()
    }

    this.create = function () {
        //tworzenie diva na mape
        contentMapDiv = document.createElement("div")
        contentMapDiv.id = "contentMapDiv"
        document.getElementById("mapDiv").append(contentMapDiv)
        //canvas create
        canvasMinimap = document.createElement('canvas');
        canvasMinimap.id = "canvasMinimap";
        canvasPlayerOnMinimap = document.createElement('canvas');
        canvasPlayerOnMinimap.id = "canvasPlayerOnMinimap";
        this.setCanvasSize()
        document.getElementById("contentMapDiv").appendChild(canvasMinimap);
        document.getElementById("contentMapDiv").appendChild(canvasPlayerOnMinimap)
        //tworzenie napisu w map divie
        playerInfo = document.createElement("h1")
        playerInfo.id = "playerInfo"
        document.getElementById("mapDiv").append(playerInfo)
    }


    this.create();
}