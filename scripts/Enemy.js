function _Enemy(type,x,y){
    //logic values
    this.dead = false
    var that = this
    this.type = Variables.enemiesTypes[0]
    this.x = x
    this.y = y
    this.range = Variables.enemiesTypes[0].range
    this.direction = 0
    this.rotation = 0
    this.rotationDeg = 0
    this.speed = 0
    this.state = this.type.standState;
    this.moveSpeed = this.type.moveSpeed;
    this.rotationSpeed = this.type.rotationSpeed;
    this.health = 100
    //render values
    this.img = this.type.img
    this.shooting = false
    this.awaken = false
    var shoot

    this.startShoot = function(){
        this.shooting = true
        this.awaken = true
        this.state = this.type.shootState[0]
        shoot = setInterval(function(){
            if(!that.dead){
                var dx = that.x - player.x;
                var dy = that.y - player.y;
                that.state == that.type.shootState[0] ? that.state = that.type.shootState[1] : that.state = that.type.shootState[0]
                player.receiveDemage()
                if(Math.pow(dx,2)+Math.pow(dy,2) > that.range){
                    clearInterval(shoot);
                    document.getElementById("faceImage").src = "img/faceImage1.png"
                    that.shooting = false
                    that.state = that.type.standState
                }
            }else
                clearInterval(shoot);   
        },200)
    }

    function checkMove(oldX, oldY, newX, newY, radius) {
        var pos = {
            x: oldX,
            y: oldY
        };

        if (newY < 0 || newY >= level.mapHeight || newX < 0 || newX >= level.mapWidth)
            return pos;

        var blockX = Math.floor(newX);
        var blockY = Math.floor(newY);


        if (collision(blockX, blockY))
            return pos;

        pos.x = newX;
        pos.y = newY;

        if (collision(blockX, blockY - 1) != 0 && newY - blockY < radius)
            pos.y = blockY + radius;
        else if (collision(blockX, blockY + 1) != 0 && blockY + 1 - newY < radius)
            pos.y = blockY + 1 - radius;
        else if (collision(blockX - 1, blockY) && newX - blockX < radius)
            pos.x = blockX + radius;
        else if (collision(blockX + 1, blockY) && blockX + 1 - newX < radius)
            pos.x = blockX + 1 - radius;

        return pos;
    }

    function collision(x, y) {
        if (((0 > y || 0 > x) || (y > level.mapHeight - 1 || x > level.mapWidth - 1)) || level.getLevel()[Math.floor(y)][Math.floor(x)] > 0)
            return true
        else
            return false
    }

    var walkStatus = 0

    this.move = function () {
        var moveStep = that.speed * that.moveSpeed;
        that.rotation += that.direction * that.rotationSpeed
        that.state = that.type.walkState[walkStatus]
        walkStatus == that.type.walkState.length-1 ? walkStatus = 0 : walkStatus++
        
        var newX = that.x + Math.cos(that.rotation) * moveStep;
        var newY = that.y + Math.sin(that.rotation) * moveStep;
        var newPosition = checkMove(that.x, that.y, newX, newY, 0.3)
        // // Set new position        
        that.x = newPosition.x;
        that.y = newPosition.y;
    }

    this.getDemage = function (demage) {
        this.health-= demage
        if(this.health <= 0){
            clearInterval(shoot)
            var deadStates = that.type.deadState.length-1
            var dead = setInterval(function(){
                    that.state = that.type.deadState[deadStates]
                    deadStates--
                    if(deadStates < 0){
                        that.dead = true
                        clearInterval(dead);
                    } 
            },100)
            player.killedEnemies++
        } 
    }
}