function _Sight() {
    var that = this
    this.x = 0
    this.y = 0
    this.enemiesRects = []
    var canvas = document.getElementById("canvasView")
    var context = canvas.getContext('2d');
    document.getElementById("canvasDiv").addEventListener("mousemove", function(e){
        that.x = e.layerX
        that.y = e.layerY
    })

    document.getElementById("canvasDiv").addEventListener("click", function(e){
        if(player.pistolAmmo > 0){
            for(var i=0; i<that.enemiesRects.length; i++){
                if( (e.layerX >= that.enemiesRects[i].left && e.layerX <= that.enemiesRects[i].left+that.enemiesRects[i].width) &&
                (e.layerY >= that.enemiesRects[i].top && e.layerY <= that.enemiesRects[i].top+that.enemiesRects[i].height) )
                    that.enemiesRects[i].enemy.getDemage(40)
            }
        }
    })

    this.drawSight = function(){
        context.fillStyle = '#E8DE2A';
        context.globalAlpha = 1
        context.fillRect(this.x-1, this.y-8, 3, 7);
        context.fillRect(this.x-1, this.y+8, 3, 7);
        context.fillRect(this.x+6, this.y+1, 7, 3);
        context.fillRect(this.x-10, this.y+1, 7, 3);
    }
}