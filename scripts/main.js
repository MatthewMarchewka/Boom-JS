var map
var view
var player
var level
var camera


document.addEventListener("DOMContentLoaded", function () {
    Variables.mapWallTexture = new Image()
    Variables.mapWallTexture.src = "img/mapWall.png"
    Variables.mapWallTexture.onload = function () {
        Variables.mapPlayerTexture = new Image()
        Variables.mapPlayerTexture.src = "img/player.png"
        Variables.mapPlayerTexture.onload = function () {
            Variables.textures = new Image()
            Variables.textures.src = "img/textures.png"
            Variables.textures.onload = function () {
                Variables.sprites = new Image()
                Variables.sprites.src = "img/sprites.png"
                Variables.sprites.onload = function () {
                    Variables.solider = new Image()
                    Variables.solider.src = "img/solider.png"
                    Variables.solider.onload = function () {
                        Variables.hit = new Audio('sounds/hit.mp3')
                        //real onload function (waiting for images)
                        player = new _Player()
                        level = new _Level()
                        map = new _Map()
                        view = new _View()
                        sight = new _Sight()
                        function gameCycle() {
                            player.move()
                            map.update()
                            bindKeys()
                            //if (player.direction != 0 || player.speed != 0)
                            view.castRays()
                            view.renderEnemies()
                            sight.drawSight()
                            setTimeout(gameCycle, 1000 / 25); // Aim for 30 FPS
                        }
                        gameCycle()
                        window.addEventListener("resize", function () {
                            view.setCanvasSize()
                            map.setCanvasSize()
                        })    // canvas fill listener
                    }
                }
            }
        }
    }
})