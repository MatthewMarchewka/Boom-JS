function _Level(){
    //0 - path
    // negative values - lobic, sprites
    // positive values - walls, doors
    var level = [
        [1, 1, 1, 1, 1, 2, 1, 1, 1, 2, 1, 1, 1, 2, 1, 1, 1, 2, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1],
        [1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1],
        [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1],
        [1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 3, 0, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1],
        [1, 1, 1, 3, 1, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, -2, 1, 1, 1, 1, 1, 0, 0, 1, 2, 0, 0, 0, 1],
        [1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 2, 1, 1, 2, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0, 0, 3],
        [1, 0, 0, 0, 1, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1],
        [1, 0, 0, 0, 1, 3, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, -3, 0, 0, 0, 0, 0, 0, 0, -4, 0, 0, 1, 1],
        [1, 1, 0, 0, 0, 0, 0, 0, 2, 1, 0, 0, 0, 1, 2, 1, 3, 1, 1, 1, 2, 1, 1, 1, 3, 1, 1, 1, 1],
        [1, 1, 1, 0, 0, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
        [1, 1, 1, 1, 0, -4, 0, 0, 2, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1, 1, 0, 0, 1, 2, 0, 1, -2, 0, 0, 1, 0, 0, 0, 3, 0, 1, 1, 1, 1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 3, 0, 0, 0, 1, 0, 1, 0, -1, 4, 1, 1, 1, 1],   //tu jest koniec giereczki
        [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 2, 1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 0, 0, -4, 0, 1, 1, 1, 1, 2, 1, 1, 0, 1, 1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 1, 1, 0, 0, 0, 0, 0, 0, 3, 1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
    ];
    //add half to position for clear positioning
    var levelEnemies = [
        { type: 0, x: 7.5, y: 1.5},
        { type: 0, x: 23.5, y: 3.5},
        { type: 0, x: 20.5, y: 16.5},
        //{ type: 0, x: 18, y: 1}
    ]

    var numberKeys = 2
    this.endLevel = false
    this.enemies = []


    this.mapWidth =  level[0].length;
    this.mapHeight = level.length;

    this.getLevel = function(){
        return level
    }
    this.setValue = function(y,x,value){
        level[y][x] = value
    }

    this.makeSoliders = function(){
        for(var i=0; i<levelEnemies.length; i++){
            this.enemies.push(new _Enemy(levelEnemies[i].type, levelEnemies[i].x, levelEnemies[i].y))
        }
    }
    this.makeSoliders()

    this.endLevelPrompt = function(komunikat){
        var endPrompt = document.createElement('div');
        endPrompt.id = "endPrompt";
        document.body.appendChild(endPrompt);

        var message = document.createElement('h1')
        message.innerHTML = komunikat+"<br>"
        endPrompt.appendChild(message)

        var keys = document.createElement('h4')
        keys.innerHTML = "You collect: " + document.getElementById("numberKeys").innerHTML+"/0"+numberKeys+" keys<br>"
        endPrompt.appendChild(keys)

        var killed = document.createElement('h4')
        killed.innerHTML = "You killed: 0" +player.killedEnemies +"/0"+levelEnemies.length+" enemies<br>" 
        endPrompt.appendChild(killed)
    }
}