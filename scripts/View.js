function _View() {
    //view logic
    var view = this
    //canvas element
    var canvasView
    var context
    var parentCanvas
    //ray variables
    var lineWidth = 2;
    var fov = 60 * Math.PI / 180;
    var numberRays = Math.ceil(Variables.canvasWidth / lineWidth);
    var viewDistance = (Variables.canvasWidth / 2) / Math.tan((fov / 2))


    this.create = function () {
        canvasView = document.createElement('canvas');
        canvasView.id = "canvasView";
        parentCanvas = document.getElementById("canvasDiv")
        //canvasView.style.border   = "1px solid red";
        context = canvasView.getContext('2d');
        this.setCanvasSize()
        parentCanvas.appendChild(canvasView);
    }

    this.setCanvasSize = function () {
        canvasView.width = parentCanvas.offsetWidth - 60            //odejmujemy 60 ponieważ tyle wynosi szerokość ramki naszego diva
        canvasView.height = parentCanvas.offsetHeight - 60
        Variables.canvasWidth = canvasView.width
        Variables.canvasHeight = canvasView.height
        numberRays = Math.ceil(Variables.canvasWidth / lineWidth);
        viewDistance = (Variables.canvasWidth / 2) / Math.tan((fov / 2));
        view.castRays()
    }

    this.castRays = function () {
        context.clearRect(0, 0, Variables.canvasWidth, Variables.canvasHeight)
        var singleRayCounter = 0
        //numberRays to wynik dzielenia szerokości canvasa na dokładność renderu (obie zmienne określone)
        for (var i = 0; i < numberRays; i++) {
            //rayScreenPos określa odległość od ściany odbijającej reya
            var rayScreenPos = (-numberRays / 2 + i) * lineWidth;
            //rayViewDist - to określenie odległości pomiędzy playerem a ścianą na ukos (wykorzystujemy pitagorasa)
            var rayViewDist = Math.sqrt(rayScreenPos * rayScreenPos + viewDistance * viewDistance);
            //rayAngle - kierunek castowanego rayu używamy arcus sinus żeby dostać wartość w kącie z wartości odległościowych
            var rayAngle = Math.asin(rayScreenPos / rayViewDist);
            view.castSingleRay(player.rotation + rayAngle, singleRayCounter++, "castRays");
        }
    }

    this.castSingleRay = function (rayAngle, rayCounter, origin) {
        //przygotowanie kąta do funkcji
        rayAngle %= Math.PI * 2;
        if (rayAngle < 0) rayAngle += Math.PI * 2;
        //sprawdzenie dla każdego reya w którym kierunku jest rzutowany
        var right = (rayAngle > Math.PI * 2 * 0.75 || rayAngle < Math.PI * 2 * 0.25);
        var up = (rayAngle < 0 || rayAngle > Math.PI);
        //miejsce zderzenia
        var hit = { wallType: 0, distance: 0, texture: 0 }
        //tablica spritów do narysowania
        var sprites = []
        //punkt w przestrzeni 2D zderzenia z rayem
        var wallX
        var wallY
        //ruch pionowy
        var slope = Math.tan(rayAngle)  //zakrzywienie ruchu pionowego
        //pozycja początkowa raya
        var x = right ? Math.ceil(player.x) : Math.floor(player.x);
        var y = player.y + (x - player.x) * slope;
        //współczynnik lotu raya
        var dX = right ? 1 : -1
        var dY = dX * slope

        while (x >= 0 && x < level.mapWidth && y >= 0 && y < level.mapHeight) {
            //sprawdzamy punkt w którym aktualnie jest ray
            var wallX = Math.floor(x + (right ? 0 : -1));
            var wallY = Math.floor(y);
            //napotkanie sprita do narysowania
            if (level.getLevel()[wallY][wallX] < -1)
                sprites.push({ spriteType: level.getLevel()[wallY][wallX], distance: Math.pow(x - player.x, 2) + Math.pow(y - player.y, 2), texture: y % 1 })
            //jeżeli w danym punkcie jest ściana kończymy pętle while i przechodzimy do rysowania na ekranie
            if (level.getLevel()[wallY][wallX] > 0) {
                hit.distance = Math.pow(x - player.x, 2) + Math.pow(y - player.y, 2)
                hit.wallType = level.getLevel()[wallY][wallX];
                hit.texture = y % 1;
                break;
            }
            //jeśli nie trafiliśmy na ścianę lecimy dalej wskaźnikami
            x += dX;
            y += dY;
        }
        //ruch poziomy
        var slope = 1 / Math.tan(rayAngle)
        var y = up ? Math.floor(player.y) : Math.ceil(player.y);
        var x = player.x + (y - player.y) * slope;
        var dY = up ? -1 : 1;
        var dX = dY * slope;

        while (x >= 0 && x < level.mapWidth && y >= 0 && y < level.mapHeight) {
            var wallY = Math.floor(y + (up ? -1 : 0));
            var wallX = Math.floor(x);

            // if (level.getLevel()[wallY][wallX] < -1)
            //     sprites.push({spriteType: level.getLevel()[wallY][wallX], distance: Math.pow(x-player.x,2) + Math.pow(y - player.y,2), texture: y % 1})

            if (level.getLevel()[wallY][wallX] > 0) {
                var blockDist = Math.pow(x - player.x, 2) + Math.pow(y - player.y, 2)
                if (hit.distance == 0 || blockDist < hit.distance) {    //sprawdzenie która oś jest bliżej
                    hit.distance = blockDist;
                    hit.wallType = level.getLevel()[wallY][wallX]
                    hit.texture = x % 1;
                }
                break;
            }
            x += dX;
            y += dY;
        }
        if (origin == "castRays") {
            if (hit.distance != 0) {
                hit.distance = Math.sqrt(hit.distance) * 2.5
                hit.distance *= Math.cos(player.rotation - rayAngle);
                var height = Math.round(viewDistance / hit.distance) * 2
                var top = Math.round((Variables.canvasHeight - height) / 2);
                //drawImage( src, srcX, srcY, srcWidth, srcHeight, realLeft, realTop, realWidth, realHeight)
                var texX = Math.round(hit.texture * 64);
                context.globalAlpha = 1
                context.drawImage(Variables.textures, texX, (hit.wallType - 1) * 64, lineWidth, 64, rayCounter * lineWidth, top, lineWidth, height)
                context.fillStyle = '#000000';
                context.globalAlpha = Math.sqrt(hit.distance / 50)
                context.fillRect(rayCounter * lineWidth, top, lineWidth, height);
            }
            if (sprites.length > 0) {
                for (var i = sprites.length - 1; i >= 0; i--) {
                    if ((Math.sqrt(sprites[i].distance) * 2.5) * Math.cos(player.rotation - rayAngle) < hit.distance) {
                        sprites[i].distance = Math.sqrt(sprites[i].distance)
                        sprites[i].distance *= Math.cos(player.rotation - rayAngle);
                        //console.log(sprites[i].distance, hit.distance)
                        var height = Math.round(viewDistance / sprites[i].distance)
                        var top = Math.round((Variables.canvasHeight - height) / 2)
                        var texX = Math.round(sprites[i].texture * 64)
                        context.globalAlpha = 1
                        context.drawImage(Variables.sprites, texX, (Math.abs(sprites[i].spriteType) - 2) * 64, lineWidth, 64, rayCounter * lineWidth, top, lineWidth, height)
                    }
                }
            }
        } else {
            return hit.distance
        }
    }

    this.renderEnemies = function () {
        sight.enemiesRects = []
        for (var i = 0; i < level.enemies.length; i++) {
            //enemy z tablicy
            var draw = true
            var enemy = level.enemies[i];
            if (!enemy.dead) {
                //odległość od playera (pitagoras)
                var dx = enemy.x - player.x;
                var dy = enemy.y - player.y;

                var dist = Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2));
                //kątem pomiędzy playerem a przeciwnikiem będzie ta różnica 
                var angle = Math.atan2(dy, dx) - player.rotation;
                // dodajemy/odejmujemy 360 stopni które nie zmieniają konta ale pozwalaja ujednolicić zakres kątów
                if (angle < -Math.PI) angle += 2 * Math.PI;
                if (angle >= Math.PI) angle -= 2 * Math.PI;
                // sprawdzamy czy przeciwnik jest wybudzony i czy jest daleko
                if (enemy.awaken && Math.pow(dx, 2) + Math.pow(dy, 2) > enemy.range) {
                    enemy.rotation = Math.atan2(dy, dx)
                    enemy.speed = -1
                    enemy.move()
                } else {
                    enemy.speed = 0;
                }
                //sprawdzamy czy przeciwnik jest w fieldOfView (60 stopni)
                if (angle > -Math.PI / 180 * 30 && angle < Math.PI / 180 * 30) {
                    //20 to losowo dobrana liczba -> zwiększa tolerancje rysowania przeciwnika
                    if (this.castSingleRay(angle, 0, "renderEnemies") + 10 < Math.pow(dx, 2) + Math.pow(dy, 2))
                        draw = false
                    else if (Math.pow(dx, 2) + Math.pow(dy, 2) < enemy.range && !enemy.shooting)
                        enemy.startShoot()
                    if (draw) {
                        //x określa o ile zmieniamy szerokość przy zmianie wysokość o 1
                        var x = Math.tan(angle) * viewDistance;
                        //określenie sizu przeciwnika
                        var width = viewDistance / (Math.cos(angle) * dist)
                        var height = viewDistance / (Math.cos(angle) * dist)
                        var top = Math.round((Variables.canvasHeight - height) / 2);    //wszystkie elementy w raycast są pozycjonowane na środku
                        var left = Variables.canvasWidth / 2 + x - width / 2
                        sight.enemiesRects.push({ enemy: enemy, left: left, top: top, width: width, height: height })
                        context.globalAlpha = 1
                        //drawImage( src, srcX, srcY, srcWidth, srcHeight, realLeft, realTop, realWidth, realHeight)
                        context.drawImage(Variables[enemy.img], enemy.type.spriteSize * enemy.state, 0, enemy.type.spriteSize, enemy.type.spriteSize, left, top, width, height)
                    }
                }
            }
        }
    }

    this.create();
}