var Variables = {
    canvasWidth: 0,
    canvasHeight: 0,
    mapWallTexture: null,
    mapPlayerTexture: null,
    textures: null,
    sprites: null,
    enemiesTypes: [
        {img: "solider", spriteSize: 64, moveSpeed: 0.07, rotationSpeed: 3, standState: 0, range: 10, walkState: [1,2,3,4], shootState: [11,12], deadState: [10,8,7,6,5]}
    ],
    solider: null,
    hit: null
}