document.getElementById("canvasDiv").addEventListener("click", function(){
    //strzelanie
});

window.onkeyup = function (e) {
    e = e || window.event;
    if(e.keyCode == 69 && level.endLevel)
        level.endLevelPrompt("YOU COMPLETE 1 LEVEL")
    else if( (e.keyCode == 49 || e.keyCode == 50) || e.keyCode == 51)
        document.getElementById("gunImage").src = "img/gun"+e.keyCode+".png"

    e.preventDefault()
}

function bindKeys() {
    document.onkeydown = function (e) {
        e = e || window.event;
        // Which key was pressed?
        switch (e.keyCode) {
            // Up, move player forward, ie. increase speed
            case 38:
                player.speed = 1; break;
            // Down, move player backward, set negative speed
            case 40:
                player.speed = -1; break;
            // Left, rotate player left
            case 37:
                player.direction = -1; break;
            // Right, rotate player right
            case 39:
                player.direction = 1; break;
        }
        e.preventDefault()
    }
    // Stop the player movement/rotation
    // when the keys are released
    document.onkeyup = function (e) {
        e = e || window.event;
        switch (e.keyCode) {
            case 38:
            case 40:
                player.speed = 0; break;
            case 37:
            case 39:
                player.direction = 0; break;
        }
        e.preventDefault()
    }
}
