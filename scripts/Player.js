function _Player() {
    this.x = 1.5
    this.y = 2.5
    this.direction = 0
    this.rotation = 0
    this.speed = 0
    this.moveSpeed = 0.14
    this.rotSpeed = 6 * Math.PI / 180
    var that = this
    var keys = 0
    this.pistolAmmo = 0
    var health = 100

    this.killedEnemies = 0

    function checkMove(oldX, oldY, newX, newY, radius) {
        var pos = {
            x: oldX,
            y: oldY
        };

        if (newY < 0 || newY >= level.mapHeight || newX < 0 || newX >= level.mapWidth)
            return pos;

        var blockX = Math.floor(newX);
        var blockY = Math.floor(newY);


        if (collision(blockX, blockY))
            return pos;

        pos.x = newX;
        pos.y = newY;

        if (collision(blockX, blockY - 1) != 0 && newY - blockY < radius)
            pos.y = blockY + radius;
        else if (collision(blockX, blockY + 1) != 0 && blockY + 1 - newY < radius)
            pos.y = blockY + 1 - radius;
        else if (collision(blockX - 1, blockY) && newX - blockX < radius)
            pos.x = blockX + radius;
        else if (collision(blockX + 1, blockY) && blockX + 1 - newX < radius)
            pos.x = blockX + 1 - radius;

        return pos;
    }

    function collision(x, y) {
        if (((0 > y || 0 > x) || (y > level.mapHeight - 1 || x > level.mapWidth - 1)) || level.getLevel()[Math.floor(y)][Math.floor(x)] > 0)
            return true
        else
            return false
    }

    this.move = function () {
        var moveStep = player.speed * player.moveSpeed;
        player.rotation += player.direction * player.rotSpeed;
        var newX = player.x + Math.cos(player.rotation) * moveStep;
        var newY = player.y + Math.sin(player.rotation) * moveStep;
        var newPosition = checkMove(player.x, player.y, newX, newY, 0.3)

        //sprawdzanie czy player stoi na "logicznym polu"
        switch(level.getLevel()[Math.floor(newY)][Math.floor(newX)]){
            case -1:
                if (keys == 0)
                    document.getElementById("playerInfo").innerHTML = "Get Key!"
                else{
                    document.getElementById("playerInfo").innerHTML = "PRESS 'E' KEY"
                    level.endLevel = true
                }
                break;
            case -2:
                keys++
                document.getElementById("numberKeys").innerHTML = "0"+keys
                level.setValue(Math.floor(newY), Math.floor(newX), 0)
                break;
            case -3:
                if(health+20 >= 100)
                    health == 100
                else
                    health+= 20
                document.getElementById("hp").innerHTML = health
                level.setValue(Math.floor(newY), Math.floor(newX), 0)
                break;
            case -4:
                that.pistolAmmo+= 8
                if(that.pistolAmmo<10)
                    document.getElementById("pistolAmmo").innerHTML = "00"+that.pistolAmmo
                else
                    document.getElementById("pistolAmmo").innerHTML = "0"+that.pistolAmmo  
                level.setValue(Math.floor(newY), Math.floor(newX), 0)
        }
        // Set new position        
        player.x = newPosition.x;
        player.y = newPosition.y;

    }

    this.receiveDemage = function(){
        health--
        document.getElementById("faceImage").src = "img/faceImage2.png"
        document.getElementById("hp").innerHTML = health
        Variables.hit.play()
        if(health < 0)
            level.endLevelPrompt("YOU DIED")

    }
}